package io.ow2.amottier.sandbox.myapp;


import io.ow2.amottier.sandbox.mylib.Speaker;

public class MyApp {

    private final Speaker speaker;

    private MyApp() {
        speaker = new Speaker();
    }

    public static void main(String[] args) {
        MyApp myApp = new MyApp();
        myApp.run();
    }

    private void run() {
        speaker.sayHello();
    }
}
